import Head from "next/head";
import styles from "../styles/Home.module.css";
import NavBar from "../components/Navbar";
import BlockPhotos from "../components/home/BlockPhotos";
import FamilyBook from "../components/home/FamilyBook";
import CountryArticle from "../components/home/CountryArticle";
import HomeActivity from "../components/home/HomeActivity";
import Image from "next/image";
import Footer from "../components/Footer";
import terasse from "../public/terasse.JPG";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Chambres d&apos;hotes Domaine de Montouvrin</title>
        <meta
          name="description"
          content="Le domaine de montouvrin est une chambre d'hotes situé dans l'indre et loire, nous possedons 2 grandes chambre allant jusqu'a 5 personnes par chambre. Nous disposons de plein d'activités au domaines, et autour, nous disposons d'une piscine, d'un terrain de pétanque et d'une table de ping-pong. Venez vous ressourcez chez nous."
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavBar />
      <main className={styles.main}>
        <section className={styles.banner}>
          <div className={styles.imagetest}>
            <Image
              objectFit="cover"
              priority="true"
              layout="fill"
              src={terasse}
              alt="terasse du domaine de montouvrin"
            />
          </div>
          <div className={styles.description}>
            <h1>Domaine de Montouvrin</h1>
            <p>
              Chambre d’hôtes à la fois rustique et moderne, à quelques pas de
              toutes les richesses de la Touraine.
            </p>
          </div>
        </section>
        <hr className={styles.hr_home} />
        <div className={styles.grid}>
          <h2 className={styles.title_section_home}>
            Une chambre d’hôtes conçues pour rendre le quotidien extraordinaire.
          </h2>
          <p className={styles.presentation_home}>
            Située dans la campagne tourangelle à Tauxigny et récemment ouverte
            en 2016, le domaine de Montouvrin est une longère en pierre, datant
            d’avant 1900 et pleine de charme. Venez vous ressourcez, profiter
            d’une tranquillité, tout en étant proche de toutes les merveilles de
            la touraine. Vous apprécierez également les nombreuses activités
            disponible sur place.
          </p>
          <hr className={styles.hr_home} />
          <section className={styles.block_photos}>
            <BlockPhotos />
          </section>
          <hr className={styles.hr_home} />
          <HomeActivity />
          <hr className={styles.hr_home} />
          <h3 className={styles.title_section_home}>
            Plus qu’un simple département.
          </h3>
          <p className={styles.citation_home}>
            Ne me demandez plus pourquoi j’aime la Touraine. Je ne l’aime ni
            comme on aime son berceau, ni comme on aime une oasis dans le
            désert. Je l’aime comme un artiste aime l’art. Je l’aime moins que
            je ne vous aime, mais sans la Touraine, peut-être ne vivrais-je
            plus.
          </p>
          <br />
          <p className={styles.citation_home}>
            Le Lys dans la vallée –{` `}
            <strong>Honoré de Balzac </strong>
          </p>
        </div>
        <CountryArticle />
        <hr className={styles.hr_home} />
        <FamilyBook />
      </main>
      <footer className={styles.footer}>
        <Footer />
      </footer>
    </div>
  );
}
