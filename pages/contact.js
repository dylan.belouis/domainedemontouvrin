import React, { useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import emailjs from "emailjs-com";
import styles from "../styles/Contact.module.css";
import Head from "next/head";
const Contact = () => {
  const [message, setMessage] = useState("");
  const [email, setEmail] = useState("");

  const sendEmail = (event) => {
    event.preventDefault();
    emailjs
      .sendForm(
        process.env.NEXT_PUBLIC_SERVICE_ID,
        process.env.NEXT_PUBLIC_TEMPLATE_ID,
        event.target,
        process.env.NEXT_PUBLIC_USER_ID
      )
      .then(
        (result) => {
          setMessage("");
          setEmail("");
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      );
  };
  const getFormValues = () => {
    console.log(message);
    console.log(email);
  };

  return (
    <div className={styles.contact_page}>
      <Head>
        <title>Contact - Domaine de Montouvrin</title>
        <meta
          name="description"
          content="Contactez le domaine de Montouvrin pour toutes questions ou demande de reservation."
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header>
        <Navbar />
      </header>
      <main className={styles.contact_main}>
        <form className={styles.form_display} onSubmit={sendEmail}>
          <h1>Contact</h1>
          <div className={styles.Contact_div_input}>
            <p>Entrez votre email</p>
            <input
              type={"email"}
              className={styles.Contact_input_item}
              label="Email"
              name="Email"
              placeholder="Votre email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className={styles.Contact_div_input}>
            <p>Entrez votre message</p>
            <textarea
              type="text"
              label="Message"
              name="Message"
              className={`${styles.Contact_input_item}, ${styles.Contact_div_textfield}`}
              placeholder="Votre message"
              value={message}
              onChange={(e) => setMessage(e.target.value)}
            />
          </div>
          <button type="submit" className={styles.Contact_submit_button}>
            Envoyer
          </button>
        </form>
      </main>
      <footer>
        <Footer />
      </footer>
    </div>
  );
};

export default Contact;
