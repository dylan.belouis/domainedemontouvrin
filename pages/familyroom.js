import Head from "next/head";
import NavBar from "../components/Navbar";
import styles from "../styles/rooms/RoomsTemplate.module.css";
import Photos from "../components/butterfly/ButterflyPhoto.js";
import Footer from "../components/Footer";
import Image from "next/image";
import banner from "../public/suite5.jpg";
const FamilyRoom = () => {
  return (
    <div className="family-room">
      <Head>
        <title>Chambre Familliale - Domaine de Montouvrin</title>
        <meta
          name="description"
          content="La chambre papillon peux accueillir jusqu'à 5 personnes, la chambre est composé d'une salle de bain, et de 3 lits dans votre prochaine chambre d'hotes préféré au domaine de montouvrin"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header>
        <NavBar />
      </header>
      <main>
        <section className={styles.banner}>
          <div className={styles.imagetest}>
            <Image
              objectFit="cover"
              priority="true"
              layout="fill"
              src={banner}
              alt="terasse du domaine de montouvrin"
            />
          </div>
          <p className={styles.description}>Chambre Familliale</p>
        </section>
        <section className={styles.room_detail}>
          <h1 className={styles.room_title}>Chambre Familliale</h1>
          <hr />
          <p className={styles.room_detail_content}>
            70 à 130 euros - Petit déjeuner compris
          </p>
          <p className={styles.room_detail_content}>1 CHAMBRES · 1 À 5 PERS</p>
          <p className={styles.room_detail_content}>
            Venez dormir dans la chambre Familliale, cette suite comprend une
            chambre avec 2 à 3 lits suivant le nombre de convives, découvrez le
            plaisir de vous réveillez dans ce confortable lit. La suite
            contient, une salle de bain avec une charmante baignoire et un
            dressing suffisant pour toutes durées de séjour. Vous trouverez
            aussi une bouilloire pour vous préparez des boissons chaude à toutes
            heures.
          </p>
        </section>
        <section className={styles.service_room_detail}>
          <div className={styles.service_room_detail_bloc_content}>
            <div className={styles.service_room_detail_content}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true"
                role="img"
                width="1em"
                height="1em"
                preserveAspectRatio="xMidYMid meet"
                viewBox="0 0 24 24"
              >
                <g
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                >
                  <rect x="2" y="7" width="20" height="15" rx="2" ry="2" />
                  <path d="M17 2l-5 5l-5-5" />
                </g>
              </svg>
            </div>
            <div className={styles.service_room_detail_content}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true"
                role="img"
                width="1em"
                height="1em"
                preserveAspectRatio="xMidYMid meet"
                viewBox="0 0 24 24"
              >
                <g
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                >
                  <path d="M18 8h1a4 4 0 0 1 0 8h-1" />
                  <path d="M2 8h16v9a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V8z" />
                  <path d="M6 1v3" />
                  <path d="M10 1v3" />
                  <path d="M14 1v3" />
                </g>
              </svg>
            </div>
            <div className={styles.service_room_detail_content}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true"
                role="img"
                width="1em"
                height="1em"
                preserveAspectRatio="xMidYMid meet"
                viewBox="0 0 24 24"
              >
                <g fill="none">
                  <path
                    d="M2 10c6-6.667 14-6.667 20 0"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M6 14c3.6-4 8.4-4 12 0"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <circle
                    cx="12"
                    cy="18"
                    r="1"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </g>
              </svg>
            </div>
            <div className={styles.service_room_detail_content}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true"
                role="img"
                width="1em"
                height="1em"
                preserveAspectRatio="xMidYMid meet"
                viewBox="0 0 512 512"
              >
                <path
                  d="M416 192a95.42 95.42 0 0 1-30.94 70.21A95.8 95.8 0 0 1 352 448H160a96 96 0 0 1 0-192h88.91A95.3 95.3 0 0 1 224 192H96a96 96 0 0 0-96 96v128a96 96 0 0 0 96 96h320a96 96 0 0 0 96-96V288a96 96 0 0 0-96-96zm-96 64a64 64 0 1 0-64-64a64 64 0 0 0 64 64zM208 96a48 48 0 1 0-48-48a48 48 0 0 0 48 48zm176-32a32 32 0 1 0-32-32a32 32 0 0 0 32 32zM160 288a64 64 0 0 0 0 128h192a64 64 0 0 0 0-128z"
                  fill="currentColor"
                />
              </svg>
            </div>
          </div>
        </section>
        <section className={styles.room_photos}>
          <Photos />
        </section>
      </main>
      <footer>
        <Footer />
      </footer>
    </div>
  );
};

export default FamilyRoom;
