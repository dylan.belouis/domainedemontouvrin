import styles from "../../styles/rooms/RoomPhotos.module.css";
import Image from "next/image";
import Image1 from "../../public/papillon2.jpg";
import Image2 from "../../public/petanque.JPG";
import Image3 from "../../public/piscine.JPG";
import Image4 from "../../public/maisonfleurie.JPG";
import Image5 from "../../public/terasse2.JPG";
import Image6 from "../../public/suite4.jpg";

const ButterflyPhoto = () => {
  return (
    <section className={styles.RoomPhoto}>
      <h1>Photos</h1>
      <div className={styles.displaying_images_RoomPhoto}>
        <a href="/papillon2.jpg" target="_blank" rel="noreferrer">
          <div className={styles.image_RoomPhoto}>
            <Image layout="fill" src={Image1} alt="exterieur" />
          </div>
        </a>
        <a href="/petanque.JPG" target="_blank" rel="noreferrer">
          <div className={styles.image_RoomPhoto}>
            <Image layout="fill" src={Image2} alt="exterieur" />
          </div>
        </a>
        <a href="/piscine.JPG" target="_blank" rel="noreferrer">
          <div className={styles.image_RoomPhoto}>
            <Image layout="fill" src={Image3} alt="exterieur" />
          </div>
        </a>
        <a href="/maisonfleurie.JPG" target="_blank" rel="noreferrer">
          <div className={styles.image_RoomPhoto}>
            <Image layout="fill" src={Image4} alt="exterieur" />
          </div>
        </a>
        <a href="/terasse2.JPG" target="_blank" rel="noreferrer">
          <div className={styles.image_RoomPhoto}>
            <Image layout="fill" src={Image5} alt="exterieur" />
          </div>
        </a>
        <a href="/suite4.jpg" target="_blank" rel="noreferrer">
          <div className={styles.image_RoomPhoto}>
            <Image layout="fill" src={Image6} alt="exterieur" />
          </div>
        </a>
      </div>
    </section>
  );
};

export default ButterflyPhoto;
