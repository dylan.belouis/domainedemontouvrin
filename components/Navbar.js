import styles from "../styles/Navbar.module.css";
import Link from "next/link";
import Image from "next/image";

const NavBar = (props) => {
  return (
    <header className={styles.header}>
      <Link href="/" passHref rel="canonical">
        <div className={styles.block_logo}>
          <div className={styles.logo}>
            {/* <Image layout="fill" src="/logo.png" alt="domaine de montouvrin" /> */}
            <h1>Domaine de Montouvrin</h1>
          </div>
        </div>
      </Link>
      <ul className={styles.headerText}>
        <Link href="/butterflyroom" passHref rel="canonical">
          <li className={styles.li_menu_item}>Papillons</li>
        </Link>
        <Link href="/contact" passHref rel="canonical">
          <li className={styles.li_menu_item}>Contact</li>
        </Link>
        <Link href="/familyroom" passHref rel="canonical">
          <li className={styles.li_menu_item}>Famillial</li>
        </Link>
        {/* <Icon icon="icon-park:hamburger-button" /> */}
      </ul>
    </header>
  );
};

export default NavBar;
