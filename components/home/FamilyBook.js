import styles from "../../styles/home/FamilyBook.module.css";
import Image from "next/image";
import family_book from "../../public/livremami.jpg";
const FamilyBook = () => {
  return (
    <section className={styles.section_family_book}>
      <div className={styles.display_family_book}>
        <div className={styles.div_image_livre}>
          <div className={styles.image_livre}>
            <Image
              objectFit="contain"
              layout="fill"
              src={family_book}
              alt="livre"
            />
          </div>
        </div>
        <div className={styles.bloc_text_family_book}>
          <h3 className={styles.text_family_book}>Fan de lecture ?</h3>
          <p className={styles.text_family_book_description}>
            Retrouvez sur place les 2 ouvrages de l’écrivaine de la famille,
            Joëlle Soyer.
          </p>
        </div>
      </div>
    </section>
  );
};

export default FamilyBook;
