import styles from "../../styles/BlockPhotos.module.css";
import Image from "next/image";
import Image1 from "../../public/exterieur.JPG";
import Image2 from "../../public/papillon2.jpg";
import Image3 from "../../public/petanque.JPG";
import Image4 from "../../public/piscine.JPG";
import Image5 from "../../public/maisonfleurie.JPG";
import Image6 from "../../public/terasse2.JPG";
import Image7 from "../../public/suite4.jpg";
import Image8 from "../../public/parc.JPG";
import Image9 from "../../public/terasse.JPG";
import Image10 from "../../public/DSC_0027.JPG";
import Image11 from "../../public/DSC_0016.JPG";

const BlockPhotos = () => {
  return (
    <div className={styles.BlockPhotos}>
      <h1>Photos</h1>
      <div className={styles.displaying_images_BlockPhotos}>
        <a href="/exterieur.JPG" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image1} alt="exterieur" />
          </div>
        </a>
        <a href="/papillon2.JPG" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image2} alt="exterieur" />
          </div>
        </a>
        <a href="/petanque.JPG" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image3} alt="exterieur" />
          </div>
        </a>
        <a href="/piscine.JPG" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image4} alt="exterieur" />
          </div>
        </a>
        <a href="/maisonfleurie.JPG" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image5} alt="exterieur" />
          </div>
        </a>
        <a href="/terasse2.JPG" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image6} alt="exterieur" />
          </div>
        </a>
        <a href="/suite4.jpg" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image7} alt="exterieur" />
          </div>
        </a>
        <a href="/beauval.jpg" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image8} alt="exterieur" />
          </div>
        </a>
        <a href="/terasse.JPG" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image9} alt="exterieur" />
          </div>
        </a>
        <a href="/DSC_0027.JPG" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image10} alt="exterieur" />
          </div>
        </a>
        <a href="/DSC_0016.JPG" target="_blank">
          <div className={styles.image_BlockPhotos}>
            <Image layout="fill" src={Image11} alt="exterieur" />
          </div>
        </a>
      </div>
    </div>
  );
};

export default BlockPhotos;
