import styles from "../../styles/home/HomeActivity.module.css";
import Image from "next/image";

const HomeActivity = () => {
  return (
    <section>
      <h3 className={styles.title_section_home}>Activités sur place</h3>
      <div className={styles.block_activities}>
        <div className={styles.grid_activities}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            role="img"
            width="2.25em"
            height="2em"
            preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 640 512"
          >
            <path
              d="M624 416h-16c-26.04 0-45.8-8.42-56.09-17.9c-8.9-8.21-19.66-14.1-31.77-14.1h-16.3c-12.11 0-22.87 5.89-31.77 14.1C461.8 407.58 442.04 416 416 416s-45.8-8.42-56.09-17.9c-8.9-8.21-19.66-14.1-31.77-14.1h-16.3c-12.11 0-22.87 5.89-31.77 14.1C269.8 407.58 250.04 416 224 416s-45.8-8.42-56.09-17.9c-8.9-8.21-19.66-14.1-31.77-14.1h-16.3c-12.11 0-22.87 5.89-31.77 14.1C77.8 407.58 58.04 416 32 416H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h16c38.62 0 72.72-12.19 96-31.84c23.28 19.66 57.38 31.84 96 31.84s72.72-12.19 96-31.84c23.28 19.66 57.38 31.84 96 31.84s72.72-12.19 96-31.84c23.28 19.66 57.38 31.84 96 31.84h16c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16zm-400-32v-96h192v96c19.12 0 30.86-6.16 34.39-9.42c9.17-8.46 19.2-14.34 29.61-18.07V128c0-17.64 14.36-32 32-32s32 14.36 32 32v16c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-16c0-52.94-43.06-96-96-96s-96 43.06-96 96v96H224v-96c0-17.64 14.36-32 32-32s32 14.36 32 32v16c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-16c0-52.94-43.06-96-96-96s-96 43.06-96 96v228.5c10.41 3.73 20.44 9.62 29.61 18.07c3.53 3.27 15.27 9.43 34.39 9.43z"
              fill="currentColor"
            />
          </svg>
          <p>Piscine</p>
        </div>
        <div className={styles.grid_activities}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            role="img"
            width="2em"
            height="2em"
            preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 24 24"
          >
            <path
              d="M12.3 2c-.97.03-1.72.84-1.69 1.8c.01.24.06.47.16.7l.29.64c.04.13-.03.27-.17.31c-.09.05-.19 0-.26-.08l-.42-.55c-.33-.42-.83-.68-1.36-.69c-.97-.02-1.77.75-1.79 1.71c-.01.42.13.82.39 1.16l.42.5h.01c.08.13.05.29-.06.37c-.09.07-.21.07-.29 0L7 7.45c-.34-.26-.75-.4-1.16-.39c-.96.02-1.73.82-1.71 1.79c.01.53.27 1.03.69 1.36l.57.44c.11.1.11.26-.01.35a.23.23 0 0 1-.26.05h-.01l-.61-.28c-.23-.09-.46-.15-.7-.16c-.96-.03-1.77.73-1.8 1.7c0 .72.4 1.38 1.06 1.66l11.39 5.07l4.59-4.59l-5.07-11.39C13.69 2.39 13 1.97 12.3 2m.83 4.1c.42-.01.8.23.96.61l3.05 6.84l-3.95-3.94l-.93-2.11c-.3-.63.16-1.38.87-1.4M9.85 8.85c.27 0 .52.1.71.3l4.81 4.81c.4.38.41 1.01.03 1.41c-.4.4-1.02.41-1.44 0l-4.81-4.81a.987.987 0 0 1-.02-1.41c.19-.2.45-.3.72-.3m-2.72 3.32c.13 0 .27.04.37.09l2.13.94l3.94 3.94l-6.86-3.05c-1.02-.44-.68-1.95.42-1.92m13.15 3.87l-4.24 4.24l.85.85c.76.75 1.86 1.04 2.89.77a3.024 3.024 0 0 0 2.12-2.12c.27-1.03-.02-2.13-.77-2.89l-.85-.85z"
              fill="currentColor"
            />
          </svg>
          <p>badminton</p>
        </div>
        <div className={styles.grid_activities}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            role="img"
            width="2em"
            height="2em"
            preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 17 16"
          >
            <g fill="currentColor" fillRule="evenodd">
              <path d="M13.297 10.802c-.451-.452-.447-.899-.372-1.144c.2-.67.474-1.415.632-2.234L8.53 12.451c.793-.162 1.517-.428 2.168-.621c.25-.074.723-.053 1.197.421c.473.475 2.906 3.642 2.906 3.642l2.164-2.163c0-.001-3.216-2.476-3.668-2.928z" />
              <path d="M13.318 6.166c-.01-1.285-.493-2.733-2.094-4.336a5.938 5.938 0 0 0-8.396 8.396c1.484 1.483 2.832 1.993 4.047 2.062l6.443-6.122zm-6.847 2.9a1.537 1.537 0 0 1-1.526-1.549c0-.854.684-1.547 1.526-1.547c.844 0 1.528.693 1.528 1.547c0 .857-.685 1.549-1.528 1.549z" />
            </g>
          </svg>
          <p>Ping-Pong</p>
        </div>
        <div className={styles.grid_activities}>
          <div className={styles.petanque_image}>
            <Image layout="fill" src="/petanque.png" alt="petanque" />
          </div>
          <p>Pétanque</p>
        </div>
        <div className={styles.grid_activities}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            role="img"
            width="2em"
            height="2em"
            preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 24 24"
          >
            <path
              d="M21.68 13.26l.36 1.97l-19.69 3.54L2 16.8l2.95-.53l-.35-1.97c-.1-.54.26-1.06.81-1.16c.54-.1 1.06.26 1.16.81l.35 1.96l9.84-1.76l-.35-1.97c-.1-.55.26-1.07.81-1.18c.54-.08 1.06.28 1.16.82l.35 1.97l2.95-.53M10.06 18.4L8 22h8l-2.42-4.23l-3.52.63z"
              fill="currentColor"
            />
          </svg>
          <p>Balançoire</p>
        </div>
        <div className={styles.grid_activities}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            role="img"
            width="2em"
            height="2em"
            preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 24 24"
          >
            <path
              d="M17 12h2L12 2L5.05 12H7l-3.9 6h6.92v4h3.96v-4H21z"
              fill="currentColor"
            />
          </svg>

          <p>Parc</p>
        </div>
      </div>
      <p>DE QUOI BIEN VOUS AMUSEZ</p>
    </section>
  );
};

export default HomeActivity;
