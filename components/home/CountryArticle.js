import styles from "../../styles/home/CountryArticle.module.css";
import Image from "next/image";
import vin from "../../public/vin.jpg";
import placeplume from "../../public/placeplume.jpg";
import chambord from "../../public/chambord.jpg";
import beauval from "../../public/beauval.jpg";

const CountryArticle = () => {
  return (
    <section className={styles.section_arround_Home}>
      <article className={styles.arround_Home}>
        <div className={styles.div_image_section_arround_Home}>
          <a href="/vin.jpg" target="_blank" rel="noreferrer">
            <div className={styles.image_vin}>
              <Image
                layout="fill"
                src={vin}
                alt="domaine viticoles autour du domaine de moontouvrin "
              />
            </div>
          </a>
        </div>

        <div className={styles.bloc_text_arround_Home}>
          <h3 className={styles.text_arround_home}>Domaines Viticoles</h3>
          <p className={styles.text_arround_home}>
            La Touraine est réputée pour son vin, du Chinon, au Vouvray, en
            passant par le Montlouis ou le Bourgueil et la Touraine fait partie
            du Vignoble du val de loire qui est aujourd’hui la troisième plus
            grande région viticole de France. Venez visitez les caves des
            producteurs locaux !
          </p>
        </div>
      </article>
      <article className={styles.arround_Home_reverse}>
        <div className={styles.div_image_section_arround_Home}>
          <a href="/placeplume.jpg" target="_blank" rel="noreferrer">
            <div className={styles.image_vin}>
              <Image
                layout="fill"
                src={placeplume}
                alt="domaine viticoles autour du domaine de moontouvrin "
              />
            </div>
          </a>
        </div>

        <div className={styles.bloc_text_arround_Home}>
          <h3 className={styles.text_arround_home}>Tours Métropole</h3>
          <p className={styles.text_arround_home}>
            Tauxigny ce trouve à seulement 20 minutes de la Métropole de Tours.
            6ème ville de france ou il fait bon vivre, elle regorge de coin à
            visiter, restaurant, bar, lieux historique ou simplement vous
            balader dans les nombreuses boutiques du centre ville le plus
            dynamique de France.
          </p>
        </div>
      </article>
      <article className={styles.arround_Home}>
        <div className={styles.div_image_section_arround_Home}>
          <a href="/chambord.jpg" target="_blank" rel="noreferrer">
            <div className={styles.image_vin}>
              <Image
                layout="fill"
                src={chambord}
                alt="domaine viticoles autour du domaine de moontouvrin "
              />
            </div>
          </a>
        </div>

        <div className={styles.bloc_text_arround_Home}>
          <h3 className={styles.text_arround_home}>Chateaux de la Loire</h3>
          <p className={styles.text_arround_home}>
            Le domaine de Montouvrin ce trouve au coeur de plus de 49 sites
            touristique de chateaux, musée ou donjon sont répertoriés en
            Indre-et-loire. Venez visitez la demeure de Francois 1er à Amboise
            ou à 2 pas, celle de Leonard de Vinci. Allez appréciez le majestueux
            chateaux de Chambord.
          </p>
        </div>
      </article>
      <article className={styles.arround_Home_reverse}>
        <div className={styles.div_image_section_arround_Home}>
          <a href="/beauval.jpg" target="_blank" rel="noreferrer">
            <div className={styles.image_vin}>
              <Image
                layout="fill"
                src={beauval}
                alt="domaine viticoles autour du domaine de moontouvrin "
              />
            </div>
          </a>
        </div>

        <div className={styles.bloc_text_arround_Home}>
          <h3 className={styles.text_arround_home}>
            Des activités à porter de main
          </h3>
          <div>
            <p className={styles.text_arround_home}>
              Le plus beau zoo d’Europe qui accueillent les Pandas à 45min
              <br />
              Le célèbre Futuroscope à 1h30
              <br />
              Le plus grand marché d’Indre-et-loire à 30 min
            </p>
            {/* <p className={styles.text_arround_home}>
              Le célèbre Futuroscope à 1h30
            </p>
            <p className={styles.text_arround_home}>
              Le plus grand marché d’Indre-et-loire à 30 min
            </p> */}
          </div>
        </div>
      </article>
    </section>
  );
};

export default CountryArticle;
