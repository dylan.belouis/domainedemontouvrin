import styles from "../styles/Footer.module.css";
import Link from "next/link";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className={styles.footer__citation}>
        <p>Votre nouvelle chambre vous attend.</p>
      </div>
      <div className={styles.footer__link}>
        <a
          href="https://www.tripadvisor.fr/Hotel_Review-g1996578-d16868217-Reviews-Domaine_de_Montouvrin-Tauxigny_Tauxigny_Saint_Bauld_Indre_et_Loire_Centre_Val_de_Loi.html"
          target="_blank"
          rel="noreferrer"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            role="img"
            width="2em"
            height="2em"
            preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 20 20"
          >
            <path
              d="M20 6.009h-2.829C15.211 4.675 12.813 4 10 4s-5.212.675-7.171 2.009H0c.428.42.827 1.34.993 2.04A4.954 4.954 0 0 0 0 11.008c0 2.757 2.243 5 5 5a4.97 4.97 0 0 0 3.423-1.375L10 17l1.577-2.366A4.97 4.97 0 0 0 15 16.01c2.757 0 5-2.243 5-5c0-1.112-.377-2.13-.993-2.96c.166-.7.565-1.62.993-2.04zm-15 8.4c-1.875 0-3.4-1.525-3.4-3.4s1.525-3.4 3.4-3.4s3.4 1.525 3.4 3.4s-1.525 3.4-3.4 3.4zm5-3.4a5.008 5.008 0 0 0-4.009-4.9C7.195 5.704 8.53 5.5 10 5.5s2.805.204 4.009.61A5.008 5.008 0 0 0 10 11.008zm5 3.4c-1.875 0-3.4-1.525-3.4-3.4s1.525-3.4 3.4-3.4s3.4 1.525 3.4 3.4s-1.525 3.4-3.4 3.4zM5 8.86c-1.185 0-2.15.964-2.15 2.15s.965 2.15 2.15 2.15s2.15-.964 2.15-2.15s-.965-2.15-2.15-2.15zm0 2.791a.65.65 0 1 1 0-1.3a.65.65 0 0 1 0 1.3zm10-2.791c-1.185 0-2.15.964-2.15 2.15s.965 2.15 2.15 2.15s2.15-.964 2.15-2.15s-.965-2.15-2.15-2.15zm0 2.791a.65.65 0 1 1 0-1.3a.65.65 0 0 1 0 1.3z"
              fill="white"
            />
          </svg>
        </a>
        <a
          href="https://www.google.com/search?q=domaine+de+montouvrin&rlz=1C1ONGR_frFR979FR979&oq=do&aqs=chrome.1.69i57j35i39j0i67j46i199i433i465i512j0i67l2j0i433i512j46i199i291i433i512j0i433i512j46i433i512.2544j0j7&sourceid=chrome&ie=UTF-8#lrd=0x47fcc313bcd3d313:0x7db951a9444e6610,3,,,"
          target="_blank"
          rel="noreferrer"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            role="img"
            width="2em"
            height="2em"
            preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 16 16"
          >
            <g fill="white">
              <path d="M15.545 6.558a9.42 9.42 0 0 1 .139 1.626c0 2.434-.87 4.492-2.384 5.885h.002C11.978 15.292 10.158 16 8 16A8 8 0 1 1 8 0a7.689 7.689 0 0 1 5.352 2.082l-2.284 2.284A4.347 4.347 0 0 0 8 3.166c-2.087 0-3.86 1.408-4.492 3.304a4.792 4.792 0 0 0 0 3.063h.003c.635 1.893 2.405 3.301 4.492 3.301c1.078 0 2.004-.276 2.722-.764h-.003a3.702 3.702 0 0 0 1.599-2.431H8v-3.08h7.545z" />
            </g>
          </svg>
        </a>
        <a
          href="https://www.facebook.com/IsabelleMeddy/"
          target="_blank"
          rel="noreferrer"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            role="img"
            width="2em"
            height="2em"
            preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 1024 1024"
          >
            <path
              d="M880 112H144c-17.7 0-32 14.3-32 32v736c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V144c0-17.7-14.3-32-32-32zm-92.4 233.5h-63.9c-50.1 0-59.8 23.8-59.8 58.8v77.1h119.6l-15.6 120.7h-104V912H539.2V602.2H434.9V481.4h104.3v-89c0-103.3 63.1-159.6 155.3-159.6c44.2 0 82.1 3.3 93.2 4.8v107.9z"
              fill="white"
            />
          </svg>
        </a>
      </div>
      <p className={styles.copyright_text}>
        Copyright ©2022dylanbelouis. All Rights Reserved.
      </p>
    </footer>
  );
};
export default Footer;
